package ito.poo.app;
import ito.poo.clases.CuerpoCeleste;
import ito.poo.clases.Ubicacion;

public class MyApp {
	static void run() {
		CuerpoCeleste c= new CuerpoCeleste("Luna", null , "Solidos");
		System.out.println (c);
		System.out.println ();

		Ubicacion u = new Ubicacion((float) 1.9733400,  41.2676200f, "12 horas", 384.400F);
		System.out.println (u);
		c.desplazamiento(0, 0);
		System.out.println ();
	
		CuerpoCeleste cl= new CuerpoCeleste("Cometas",null, "Gas");
		System.out.println (cl);
		System.out.println ();

		Ubicacion ul = new Ubicacion((float) 57.22333333, 21.87305556f, "6 meses", 12.05664F);
		System.out.println (ul);
		c.desplazamiento(1, 789);
	}
	
	public static void main(String[] args) {
		run();
	}
}