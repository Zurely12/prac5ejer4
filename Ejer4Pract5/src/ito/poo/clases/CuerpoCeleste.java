package ito.poo.clases;
import java.util.HashSet;
public class CuerpoCeleste {
	
	private String nombre=  " ";
	private HashSet<Ubicacion> localizacion;
	private String composicion=  " ";
	
	public CuerpoCeleste(String nombre, HashSet<Ubicacion> localizacion, String composicion) {
		super();
		this.nombre = nombre;
		this.localizacion = localizacion;
		this.composicion = composicion;
	}
public float desplazamiento(int i, int j  ) {
		
	float  desplazamiento=0f;
	desplazamiento=(float) (Math.sqrt(Math.pow(i,2)+Math.pow(j, 2)));
	if(desplazamiento==0)
		System.out.println("-1");
	else
		System.out.println("El desplazamiento es de:"+desplazamiento);
	
return (float) desplazamiento;
}
	
public float desplazamiento(int j ) {
	float  desplazamiento=j;
	return desplazamiento;
	
}

	public HashSet<Ubicacion> getLocalizacion() {
		return this.localizacion;
}

	public void setLocalizacion(HashSet<Ubicacion> localizacion) {
		this.localizacion = localizacion;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	public String getNombre() {
		return nombre;
	}
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", localizacion=" + localizacion + ", composicion=" + composicion
				+ "]";
	}
}
